package co.simplon.livecoding;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LivecodingApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(LivecodingApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Le programme est exécuté " + getRandomNumber());
	}

	int getRandomNumber() {
		return 1;
	}

}
